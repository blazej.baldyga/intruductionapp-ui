export * from './authorREST.service';
import { AuthorRESTAPIService } from './authorREST.service';
export * from './bookREST.service';
import { BookRESTAPIService } from './bookREST.service';
export * from './default.service';
import { DefaultAPIService } from './default.service';
export const APIS = [AuthorRESTAPIService, BookRESTAPIService, DefaultAPIService];
