import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import { BookDTO } from '../../../generated';
import { Column } from '../../../core/types/column';
import { ColumnType } from 'src/app/core/types/columnType';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent {
  @Input() public results: BookDTO[];
  @Output() deleteEvent: EventEmitter<any> = new EventEmitter();

  public helpArticleId = 'PAGE_BOOKING_SEARCH';

  public deleteDialogVisible = false;
  public selectedBooking = null;
  public readonly pathToTranslationJSON =
    'BOOK_MENU.BOOK_SEARCH.BOOK.HEADER.';

  public columns: Column[] = [
    {
      field: 'title',
      header: this.pathToTranslationJSON + 'TITLE',
      type: ColumnType.INPUT
    },
    {
      field: 'isbn',
      header: this.pathToTranslationJSON + 'ISBN',
      type: ColumnType.INPUT
    },
    {
      field: 'numOfPages',
      header: this.pathToTranslationJSON + 'NUM_OF_PAGES',
      type: ColumnType.INPUT
    },
    {
      field: 'authorDTO',
      header: this.pathToTranslationJSON + 'AUTHOR_ID',
      type: ColumnType.DROPDOWN
    },
    {
      field: 'bookCategory',
      header: this.pathToTranslationJSON + 'BOOK_CATEGORY',
      type: ColumnType.DROPDOWN
    },
    {
      field: 'available',
      header: this.pathToTranslationJSON + 'IS_AVAILABLE',
      type: ColumnType.CHECKBOX
    },
  ];

  public getAuthorName(author): string {
    return author.name;
  }
  public getAuthorLastName(author): string {
    return author.lastName;
  }
  public getSelectedBook(): BookDTO {
    return this.selectedBooking;
  }

  public setSelectedBook(selectedBooking: BookDTO): void {
    this.selectedBooking = selectedBooking;
  }
}
