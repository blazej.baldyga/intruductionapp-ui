import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  Input,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  BookCategory,
  GetBooksByCriteriaRequestParams,
  AuthorDTO,
  AuthorRESTAPIService,
} from '../../../generated';
import { MessageService, SelectItem } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-criteria',
  templateUrl: './criteria.component.html',
  styleUrls: ['./criteria.component.css']
})
export class CriteriaComponent implements OnInit {

  @Input() criteria: GetBooksByCriteriaRequestParams;
  @Output() public criteriaSubmitted =
    new EventEmitter<GetBooksByCriteriaRequestParams>();
  @Output() public resetEmitter = new EventEmitter();

  public helpArticleId = 'PAGE_BOOK_SEARCH';
  public translatedData: Record<string, string>;

  public criteriaGroup: FormGroup;
  public authorsNames: SelectItem[];
  public authorsLastNames: SelectItem[];
  public bookCategory: SelectItem[];
  public canceledOptions: SelectItem[];
  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authorRestApi: AuthorRESTAPIService,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService
  ) {}
  ngOnInit(): void {this.translateService
    .get([
      'GENERAL.YES',
      'GENERAL.NO'
    ])
    .subscribe((data) => {
      this.translatedData = data;
      this.canceledOptions = [
        { label: this.translatedData['GENERAL.YES'], value: true },
        { label: this.translatedData['GENERAL.NO'], value: false },
      ];
    });
  this.initFormGroup();
  this.loadAllAuthorsName();
  this.loadAllBookCategories();
  this.loadAllAuthorsLastName();
  }
  public submitCriteria(): void {
    this.criteriaSubmitted.emit(this.criteriaGroup.value);
  }

  public resetFormGroup(): void {
    this.resetEmitter.emit();
  }

  private initFormGroup(): void {
    this.criteriaGroup = this.formBuilder.group({
      title: null,
      isbn: null,
      numOfPages: null,
      name: null,
      lastName: null,
      bookCategory: null,
      isAvailable: true,
    });
  }
  private loadAllAuthorsName(): void {
    this.authorRestApi.getAuthorsByCriteria({}).subscribe((pageResult) => {
      const authors = pageResult.stream as AuthorDTO[];
      this.authorsNames = authors.map((author) => ({
        label: 'name',
        value: author.name,
      }));
    });
  }
  private loadAllAuthorsLastName(): void {
    this.authorRestApi.getAuthorsByCriteria({}).subscribe((pageResult) => {
      const authors = pageResult.stream as AuthorDTO[];
      this.authorsLastNames = authors.map((author) => ({
        label: 'lastName',
        value: author.lastName,
      }));
    });
  }
  private loadAllBookCategories(): void {
    this.bookCategory = Object.keys(BookCategory).map((key) => ({
      label: BookCategory[key],
      value: BookCategory[key],
    }));
  }
}
