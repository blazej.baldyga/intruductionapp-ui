import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ReactiveFormsModule} from '@angular/forms';
import { CriteriaComponent } from './criteria.component';
import {Pipe, PipeTransform} from '@angular/core';
import { BookCategory, GetBooksByCriteriaRequestParams } from 'src/app/generated';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { CalendarModule } from 'primeng/calendar';
import { InputNumberModule } from 'primeng/inputnumber';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateServiceMock } from 'src/app/test/TranslateServiceMock';
import { MessageService } from 'primeng/api';

describe('CriteriaComponent', () => {
  let component: CriteriaComponent;
  let fixture: ComponentFixture<CriteriaComponent>;

  @Pipe({name: 'translate'})
  class TranslatePipeMock implements PipeTransform {
    transform(value: string): string {
      return '';
    }
  }

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CriteriaComponent, TranslatePipeMock ],
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        DropdownModule,
        CheckboxModule,
        InputNumberModule,
        CalendarModule,
        TranslateModule
      ],
      providers: [
        { provide: TranslateService, useClass: TranslateServiceMock },
        MessageService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriteriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create form', () => {
    expect(component.criteriaGroup).toBeTruthy();
  });
  it('should have predefine inputs', () => {
    expect(component.criteriaGroup.value.title).toBeNull();
    expect(component.criteriaGroup.value.author.lastName).toBeNull();
    expect(component.criteriaGroup.value.author.Name).toBeNull();
    expect(component.criteriaGroup.value.bookCategory).toBeNull();
    expect(component.criteriaGroup.value.isAvailable).toBeTrue();
  });
  it('should set inputs in search criteria', () => {
    const testCriteria: GetBooksByCriteriaRequestParams = {
      title: 'name',
      isAvailable: true,
      bookCategory: BookCategory.ACTIONANDADVENTURE
    };
    expect(component.criteriaGroup.value.title.setValue(testCriteria.title));
    expect(component.criteriaGroup.value.isAvailable.setValue(testCriteria.isAvailable));
    expect(component.criteriaGroup.value.bookCategory.setValue(testCriteria.bookCategory));
    fixture.detectChanges();

    expect(component.criteriaGroup.value.bookName).toEqual(testCriteria.title);
    expect(component.criteriaGroup.value.isAvailable).toEqual(testCriteria.isAvailable);
    expect(component.criteriaGroup.value.bookCategory).toEqual(testCriteria.bookCategory);
  });

  it('should submit search criteria', () => {
    const emitterSpy: jasmine.Spy = spyOn<any>(
      component.criteriaSubmitted,
      'emit'
    );
    component.criteriaGroup.patchValue({title: 'title', authorId: 'authorId'});
    component.submitCriteria();
    expect(emitterSpy).toHaveBeenCalledTimes(1);
    expect(emitterSpy).toHaveBeenCalledWith(component.criteriaGroup.value);
  });

  it('should reset criteria inputs', () => {
    component.criteriaGroup.patchValue({title: 'title', authorId: 'authorId'});
    component.submitCriteria();
    expect(component.criteriaGroup.value.title).toEqual('title');
    expect(component.criteriaGroup.value.authorId).toEqual('authorId');
    component.resetFormGroup();
    expect(component.criteriaGroup.value.title).toBeNull();
    expect(component.criteriaGroup.value.authorId).toBeNull();
  });
});

