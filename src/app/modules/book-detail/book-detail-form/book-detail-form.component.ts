import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MessageService, SelectItem } from 'primeng/api';
import { concatMap } from 'rxjs/operators';
import {
  BookDTO,
  BookCreateUpdateDTO,
  AuthorDTO,
  BookRESTAPIService,
  AuthorRESTAPIService,
  GetBookByIdRequestParams,
  BookCategory,
  AuthorStatus
} from 'src/app/generated';

@Component({
  selector: 'app-book-detail-form',
  templateUrl: './book-detail-form.component.html',
  styleUrls: ['./book-detail-form.component.css']
})
export class BookDetailFormComponent implements OnInit {

  @Output()
  formSubmitted: EventEmitter<any> = new EventEmitter();

  public bookToEditId: number;
  public authorId: SelectItem[];
  public bookCategory: SelectItem[];
  public options: SelectItem[];

  public editCreateFormGroup: FormGroup;
  public helpArticleId = "PAGE_BOOK_DETAIL";
  public translatedData: Record<string,string>

  constructor(
    private readonly bookRestApi: BookRESTAPIService,
    private readonly authorRestApi: AuthorRESTAPIService,
    private readonly translateService: TranslateService,
    private readonly route: ActivatedRoute,
    private readonly messageService: MessageService,
    private fb: FormBuilder
  ) { 
    this.bookToEditId = Number(this.route.snapshot.paramMap.get('id'));
    this.editCreateFormGroup = fb.group({
      title: new FormControl(null, [Validators.required]),
      isbn: new FormControl(null),
      numOfPages: new FormControl(null, [Validators.min(1)]),
      authorId: new FormControl(null, [Validators.required]),
      bookCategory: new FormControl(null, [Validators.required]),
      available: new FormControl(null, [Validators.required]),
    });
  }

  ngOnInit(): void {
    const params: GetBookByIdRequestParams= {id: this.bookToEditId};
    this.translateService.get([
      'GENERAL.YES',
      'GENERAL.NO',
      'BOOK_MENU.BOOK_CREATE_EDIT.FORM_INVALID'
    ])
    .subscribe((data) => {
      this.translatedData = data;
    });
    this.loadCategory();
    if(this.route.snapshot.paramMap.get('id')){
      this.loadData(params);
    }
    else{
      this.loadAuthors();
    }
    this.options = [
      { label: this.translatedData['GENERAL.YES'], value: true },
      { label: this.translatedData['GENERAL.NO'], value: false }
    ];
  }

  public fillForm(bookDTO: BookDTO): void{
    Object.keys(this.editCreateFormGroup.controls).forEach(element => {
      if(bookDTO[element]){
        this.editCreateFormGroup.controls[element].setValue(
          bookDTO[element]
        );
      }
      if(bookDTO.authorDTO.id){
        this.editCreateFormGroup.controls.authorId.setValue(bookDTO.authorDTO.id);
      }
    });
  }
  public emitForm(): void{
    if(this.editCreateFormGroup.valid){
      const bookCreateUpdateDTO: BookCreateUpdateDTO =
        Object.assign({},this.editCreateFormGroup.value);
        this.formSubmitted.emit(bookCreateUpdateDTO);
    }
    else{
      this.messageService.add({
        severity: 'error',
        summary: this.translatedData[
          'BOOK_MENU.BOOK_CREATE_EDIT.FORM_INVALID'
        ]
      });
    }
  }

  private loadCategory(): void{
    this.bookCategory = Object.keys(BookCategory).map(key => ({
      label: BookCategory[key],
      value: BookCategory[key]
    }));
  }
  private loadData(params: GetBookByIdRequestParams): void{
    const observableOfBook = this.bookRestApi.getBookById(params);
    this.authorRestApi
      .getAuthorsByCriteria({})
      .pipe(
          concatMap(response => {
            const authors = response.stream as AuthorDTO[];
            this.authorId = authors.map(author => ({
              label: author.id,
              value: author.id
            }));
            return observableOfBook;
          })
      )
      .subscribe(response => {
        this.fillForm(response);
      })
  }

  private loadAuthors(): void{
    this.authorRestApi.getAuthorsByCriteria({}).subscribe(response => {
      const authors = response.stream as AuthorDTO[];
      this.authorId = authors.map(author => ({
        label: author.id,
        value: author.id
      }));
    });
  }
}
