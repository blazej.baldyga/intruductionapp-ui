import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BreadcrumbService, CollapsibleDirective } from 'portal-lib';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Column } from 'src/app/core/types/column';
import { ColumnType } from 'src/app/core/types/columnType';
import { GetAuthorsByCriteriaRequestParams, AuthorDTO, AuthorRESTAPIService } from 'src/app/generated';


@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.css']
})
export class AuthorListComponent implements OnInit {

  @Input()
  public results: AuthorDTO[];
  public criteria: GetAuthorsByCriteriaRequestParams;
  public deleteDialogVisible = false;
  public helpArticleId = 'PAGE_AUTHOR_SEARCH';
  public translatedData: Record<string, string>;

  public columns: Column[] =[
    {
      field: 'name',
      header: 'AUTHOR_MENU.HEADER.NAME',
      type: ColumnType.INPUT
    },
    {
      field: 'lastName',
      header: 'AUTHOR_MENU.HEADER.LASTNAME',
      type: ColumnType.INPUT
    },
    {
      field: 'age',
      header: 'AUTHOR_MENU.HEADER.AGE',
      type: ColumnType.INPUT
    },
    {
      field: 'authorStatus',
      header: 'AUTHOR_MENU.HEADER.AUTHOR_STATUS',
      type: ColumnType.DROPDOWN
    }
  ];

  private selectedAuthor = null;

  constructor(
    private readonly authorApi: AuthorRESTAPIService,
    private translateService: TranslateService,
    private messageService: MessageService,
    private breadCrumbService: BreadcrumbService
  ) { 

  }

  ngOnInit(): void {
    this.translateService.get([
      'AUTHOR_MENU.DELETE.DELETE_SUCCESS',
      'AUTHOR_MENU.DELETE.DELETE_ERROR',
      'AUTHOR_MENU.HEADERS.LIST'
    ])
    .subscribe((data) => {
      this.translatedData = data;
      this.breadCrumbService.setItems([
        {
          title: this.translatedData['AUTHOR_MENU.HEADERS.LIST'],
          label: this.translatedData['AUTHOR_MENU.HEADERS.LIST']
        }
      ]);
    });
    this.loadData();
  }

  public getSelectedAuthor(): AuthorDTO{
    return this.selectedAuthor;
  }

  public setSelectedAuthor(selectedAuthor: AuthorDTO): void{
    this.selectedAuthor = selectedAuthor;
  }

  public deleteAuthor(id: string): void{
    this.authorApi
      .deleteAuthor({id})
      .subscribe(
        () => {
          this.messageService.add({
            severity: 'success',
            summary: this.translatedData[
              'AUTHOR_MENU.DELETE.DELETE_SUCCESS'
            ],
          });
          this.loadData();
        },
        () => {
          this.messageService.add({
            severity: 'error',
            summary: this.translatedData[
              'AUTHOR_MENU.DELETE.DELETE_ERROR'
            ],
          });
        }
      );
  }

  private loadData(): void{
    this.getAuthors().subscribe(authors => {
      this.results = authors;
    })
  }

  private getAuthors(): Observable<AuthorDTO[]>{
    return this.authorApi.getAuthorsByCriteria({})
    .pipe(map((data) => data.stream as AuthorDTO[]));
  }
}
