import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BreadcrumbService } from 'portal-lib';
import { MessageService } from 'primeng/api';
import { CreateAuthorRequestParams, AuthorDTO, AuthorRESTAPIService, AuthorCreateUpdateDTO } from 'src/app/generated';
import { AuthorDetailFormComponent } from '../author-detail-form/author-detail-form.component';

@Component({
  selector: 'app-author-create',
  templateUrl: './author-create.component.html',
  styleUrls: ['./author-create.component.css']
})
export class AuthorCreateComponent implements OnInit {

  @ViewChild(AuthorDetailFormComponent, {static: false})
  public authorDetailFormComponent: AuthorDetailFormComponent;
  public translatedData: Record<string, string>;

  public helpArticleId = "PAGE_AUTHOR_CREATE";

  constructor(
    private readonly authorApi: AuthorRESTAPIService,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
    private readonly breadCrumbService: BreadcrumbService
  ) { }

  ngOnInit(): void {
    this.translateService.get([
      'AUTHOR_MENU.CREATE.CREATE_SUCCESS',
      'AUTHOR_MENU.CREATE.CREATE_ERROR',
      'AUTHOR_MENU.HEADERS.CREATE'
    ])
    .subscribe((data) => {
      this.translatedData = data;
      this.breadCrumbService.setItems([
        {
        title: this.translatedData['AUTHOR_MENU.HEADERS.CREATE'],
        label: this.translateService['AUTHOR_MENU.HEADERS.CREATE']
        }
      ]);
    });
  }

  public onSubmit(data: AuthorDTO):void{
    const params: CreateAuthorRequestParams ={
      authorCreateUpdateDTO: data,
    };
    this.authorApi.createAuthor(params).subscribe(
      () => {
        this.messageService.add({
          severity: 'success',
          summary: this.translatedData['AUTHOR_MENU.CREATE.CREATE_SUCCESS']
        });
      },
      () => {
        this.messageService.add({
          severity:'error',
          summary: this.translatedData['AUTHOR_MENU.CREATE.CREATE.CREATE_ERROR']
        });
      }
    );
  }

}
