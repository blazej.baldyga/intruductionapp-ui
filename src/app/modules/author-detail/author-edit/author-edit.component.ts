import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BreadcrumbService } from 'portal-lib';
import { MessageService } from 'primeng/api';
import { AuthorCreateUpdateDTO, AuthorDTO, AuthorRESTAPIService, UpdateAuthorRequestParams } from 'src/app/generated';
import { AuthorDetailFormComponent } from '../author-detail-form/author-detail-form.component';


@Component({
  selector: 'app-author-edit',
  templateUrl: './author-edit.component.html',
  styleUrls: ['./author-edit.component.css']
})
export class AuthorEditComponent implements OnInit {

  @ViewChild(AuthorDetailFormComponent, {static: false})
  public authorDetailFromComponent: AuthorDetailFormComponent;
  public helpArticleId = "PAGE_AUTHOR_EDIT";
  public translatedData: Record<string, string>;
  public authorId: string;

  constructor(
    private readonly authorApi: AuthorRESTAPIService,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService,
    private readonly route: ActivatedRoute,
    private readonly breadCrumbService: BreadcrumbService
  ) {    this.authorId = String(this.route.snapshot.paramMap.get('id')); }

  ngOnInit(): void {
    this.translateService.get([
      'AUTHOR_MENU.EDIT.UPDATE_SUCCESS',
      'AUTHOR_MENU.EDIT.UPDATE_ERROR',
      'AUTHOR_MENU.HEADERS.EDIT'
    ])
    .subscribe((data) => {
      this.translatedData = data;
      this.breadCrumbService.setItems([
        {
          title: this.translatedData['AUTHOR_MENU.HEADERS.EDIT'],
          label: this.translatedData['AUTHOR_MENU.HEADERS.EDIT']
        }
      ]);
    });

  }

  public onSubmit(data: AuthorCreateUpdateDTO): void{
    console.log(this.authorId);
    const params: UpdateAuthorRequestParams ={
      id : this.authorId,
      authorCreateUpdateDTO: data,
    };
    console.log(params);
    this.authorApi
      .updateAuthor(params)
      .subscribe(
        () => {
          this.messageService.add({
            severity: 'success',
            summary: this.translatedData[
              'AUTHOR_MENU.EDIT.UPDATE_SUCCESS'
            ],
          });
        },
        () => {
          this.messageService.add({
            severity: 'error',
            summary: this.translatedData[
              'AUTHOR_MENU.EDIT.UPDATE_ERROR'
            ],
          });
        }
      );
  }
}
