import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthorDetailRoutingModule } from './author-detail-routing.module';
import { AuthorCreateComponent } from './author-create/author-create.component';
import { AuthorDetailFormComponent } from './author-detail-form/author-detail-form.component';
import { AuthorEditComponent } from './author-edit/author-edit.component';
import { AuthorListComponent } from './author-list/author-list.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [AuthorCreateComponent, AuthorDetailFormComponent, AuthorEditComponent, AuthorListComponent],
  imports: [
    CommonModule,
    AuthorDetailRoutingModule,
    SharedModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AuthorDetailModule { }
