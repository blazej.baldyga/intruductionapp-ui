import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Form, FormBuilder, FormControl, FormGroup, Validators, ReactiveFormsModule  } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MessageService, SelectItem } from 'primeng/api';
import { AuthorCreateUpdateDTO, AuthorDTO, AuthorRESTAPIService, AuthorStatus } from 'src/app/generated';

@Component({
  selector: 'app-author-detail-form',
  templateUrl: './author-detail-form.component.html',
  styleUrls: ['./author-detail-form.component.css']
})
export class AuthorDetailFormComponent implements OnInit {

  @Output()
  public formSubmitted: EventEmitter<AuthorDTO> = new EventEmitter()
  public authorForm: FormGroup;
  public authorId: string;
  public authorStatus: SelectItem[];
  public helpArticleId = "PAGE_AUTHOR_DETAIL";
  public translatedData: Record<string, string>;
  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly route: ActivatedRoute,
    private readonly authorApi: AuthorRESTAPIService,
    private readonly translateService: TranslateService,
    private readonly messageService: MessageService
  ) {
    this.authorId = String(this.route.snapshot.paramMap.get("id"));
    this.authorForm = formBuilder.group({
      name: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      age: new FormControl(0),
      authorStatus: new FormControl(null, Validators.required)
    })
   }

   ngOnInit(): void {
    this.translateService
      .get(['AUTHOR_MENU.FORM_INVALID'])
      .subscribe((data) => {
        this.translatedData = data;
      });
      this.loadStatuses();
    if (this.route.snapshot.paramMap.get('id')) {
      this.loadData(this.authorId);
    }
  }

  public emitForm(): void {

    if (this.authorForm.valid) {
      const authorDTO: AuthorDTO = this.authorForm.value;
      this.formSubmitted.emit(authorDTO);
    } else {
      this.messageService.add({
        severity: 'error',
        summary: this.translatedData['AUTHOR_MENU.FORM_INVALID'],
      });
    }
  }

  private fillForm(authorDTO: AuthorDTO): void {
    Object.keys(this.authorForm.controls).forEach((element) => {
      if (authorDTO[element]) {
        this.authorForm.controls[element].setValue(authorDTO[element]);
      }
    });
  }

  private loadData(authorGuid: string): void {
    this.authorApi.getAuthorById({ id: authorGuid }).subscribe((authors) => {
      this.fillForm(authors);
    });
  }

  private loadStatuses(): void {
    this.authorStatus = Object.keys(AuthorStatus).map(key => ({
      label: AuthorStatus[key],
      value: key
    }));
  }

}
