---
openapi: 3.0.3
info:
  title: my-library
  description: my library example application
  version: "1.0"
servers:
- url: http://localhost:8080/
tags:
- name: Author REST
- name: Book REST
paths:
  /author:
    get:
      tags:
      - Author REST
      description: Get Authors by Criteria
      operationId: getAuthorsByCriteria
      parameters:
      - name: AuthorStatus
        in: query
        schema:
          $ref: '#/components/schemas/AuthorStatus'
      - name: age
        in: query
        schema:
          format: int32
          type: integer
      - name: lastName
        in: query
        schema:
          type: string
      - name: name
        in: query
        schema:
          type: string
      - name: page
        in: query
        schema:
          format: int32
          default: "0"
          minimum: 0
          type: integer
      - name: size
        in: query
        schema:
          format: int32
          default: "100"
          maximum: 100
          minimum: 1
          type: integer
      responses:
        "500":
          description: "Internal Server Error, please check Problem Details"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "200":
          description: The corresponding Authors resources
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PageResultDTO'
    post:
      tags:
      - Author REST
      description: Create Author
      operationId: createAuthor
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/AuthorCreateUpdateDTO'
      responses:
        "201":
          description: Created Author resource
          headers:
            Location:
              description: URL for the created Author resource
              style: simple
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AuthorDTO'
        "400":
          description: Bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "500":
          description: "Internal Server Error, please check Problem Details"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
  /author/{id}:
    get:
      tags:
      - Author REST
      description: Gets Author by ID
      operationId: getAuthorById
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: string
      responses:
        "404":
          description: Not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "500":
          description: "Internal Server Error, please check Problem Details"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "200":
          description: The corresponding Author resource
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AuthorDTO'
    put:
      tags:
      - Author REST
      description: Update Author
      operationId: updateAuthor
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/AuthorCreateUpdateDTO'
      responses:
        "400":
          description: Invalid request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "404":
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "500":
          description: "Internal Server Error, please check Problem Details"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "200":
          description: Updated Author resource
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AuthorDTO'
    delete:
      tags:
      - Author REST
      description: Delete Author
      operationId: deleteAuthor
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: string
      responses:
        "204":
          description: Deleted Author by Id
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AuthorDTO'
        "404":
          description: Not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "500":
          description: "Internal Server Error, please check Problem Details"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
  /book:
    get:
      tags:
      - Book REST
      description: Gets book by criteria
      operationId: getBooksByCriteria
      parameters:
      - name: bookCategory
        in: query
        schema:
          $ref: '#/components/schemas/BookCategory'
      - name: name
        in: query
        schema:
          type: string
      - name: isAvailable
        in: query
        schema:
          type: boolean
      - name: lastName
        in: query
        schema:
          type: string
      - name: page
        in: query
        schema:
          format: int32
          default: "0"
          minimum: 0
          type: integer
      - name: size
        in: query
        schema:
          format: int32
          default: "100"
          maximum: 100
          minimum: 1
          type: integer
      - name: title
        in: query
        schema:
          type: string
      responses:
        "500":
          description: "Internal Server Error, please check Problem Details"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "200":
          description: The corresponding book resources
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PageResultDTO'
    post:
      tags:
      - Book REST
      description: Create the book
      operationId: createBook
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/BookCreateUpdateDTO'
      responses:
        "201":
          description: Created book resource
          headers:
            Location:
              description: URL for the cereated book resource
              style: simple
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BookDTO'
        "400":
          description: Bad request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "500":
          description: "Internal Server Error, please check Problem Details"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
  /book/{id}:
    get:
      tags:
      - Book REST
      description: Gets book by Id
      operationId: getBookById
      parameters:
      - name: id
        in: path
        required: true
        schema:
          format: int64
          type: integer
      responses:
        "404":
          description: Not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "500":
          description: "Internal Server Error, please check Problem Details"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BookDTO'
    put:
      tags:
      - Book REST
      description: Update Book
      operationId: updateBook
      parameters:
      - name: id
        in: path
        required: true
        schema:
          format: int64
          type: integer
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/BookCreateUpdateDTO'
      responses:
        "400":
          description: Invalid request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "404":
          description: Not Found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "500":
          description: "Internal Server Error, please check Problem Details"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "200":
          description: Updated Book resource
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BookDTO'
    delete:
      tags:
      - Book REST
      description: Delete Book
      operationId: deleteBook
      parameters:
      - name: id
        in: path
        required: true
        schema:
          format: int64
          type: integer
      responses:
        "204":
          description: Deleted Book by Id
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BookDTO'
        "404":
          description: Not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
        "500":
          description: "Internal Server Error, please check Problem Details"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RFCProblemDTO'
  /hello:
    get:
      responses:
        "200":
          description: OK
          content:
            text/plain:
              schema:
                type: string
components:
  schemas:
    OffsetDateTime:
      format: date-time
      type: string
    AuthorDTO:
      type: object
      properties:
        creationDate:
          $ref: '#/components/schemas/OffsetDateTime'
        creationUser:
          type: string
        modificationDate:
          $ref: '#/components/schemas/OffsetDateTime'
        modificationUser:
          type: string
        version:
          format: int32
          type: integer
        id:
          type: string
        age:
          format: int32
          description: Age of the author
          minimum: 1
          type: integer
        authorStatus:
          allOf:
          - $ref: '#/components/schemas/AuthorStatus'
          - description: Current status of the author
        lastName:
          description: Last name of the author
          type: string
        name:
          description: Name of the author
          type: string
    BookCategory:
      enum:
      - ACTION_AND_ADVENTURE
      - CLASSICS
      - COMIC_BOOK_OR_GRAPHIC_NOVEL
      - DETECTIVE_AND_MISTERY
      - FANTASY
      - HISTORICAL_FICTION
      - HORROR
      - LITEARY_FICTION
      type: string
    AuthorStatus:
      enum:
      - ALIVE
      - DECEASED
      type: string
    BookDTO:
      type: object
      properties:
        creationDate:
          $ref: '#/components/schemas/OffsetDateTime'
        creationUser:
          type: string
        modificationDate:
          $ref: '#/components/schemas/OffsetDateTime'
        modificationUser:
          type: string
        version:
          format: int32
          type: integer
        id:
          format: int64
          type: integer
        authorDTO:
          allOf:
          - $ref: '#/components/schemas/AuthorDTO'
          - description: Author of the book
        bookCategory:
          allOf:
          - $ref: '#/components/schemas/BookCategory'
          - description: Category of the book
        iSBN:
          description: ISBN of the book
          type: string
        isAvailable:
          description: Status wheather teh book is available right now
          type: boolean
        numOfPages:
          format: int32
          description: Number of pages in the book
          minimum: 1
          type: integer
        title:
          description: Title of the book
          type: string
        available:
          type: boolean
    RFCProblemDetailDTO:
      type: object
      properties:
        code:
          type: string
        message:
          type: string
        messageId:
          type: string
        messageType:
          type: string
    ListRFCProblemDetailDTO:
      type: array
      items:
        $ref: '#/components/schemas/RFCProblemDetailDTO'
    RFCProblemDTO:
      type: object
      properties:
        detail:
          type: string
        instance:
          type: string
        problems:
          $ref: '#/components/schemas/ListRFCProblemDetailDTO'
        status:
          format: int32
          type: integer
        title:
          type: string
        type:
          type: string
    ListObject:
      type: array
      items:
        type: object
    PageResultDTO:
      type: object
      properties:
        number:
          format: int32
          type: integer
        size:
          format: int32
          type: integer
        stream:
          $ref: '#/components/schemas/ListObject'
        totalElements:
          format: int64
          type: integer
        totalPages:
          format: int64
          type: integer
    BookCreateUpdateDTO:
      type: object
      properties:
        authorId:
          description: Author of the book
          type: string
        bookCategory:
          allOf:
          - $ref: '#/components/schemas/BookCategory'
          - description: Category of the book
        iSBN:
          description: ISBN of the book
          type: string
        isAvailable:
          description: Status wheather teh book is available right now
          type: boolean
        numOfPages:
          format: int32
          description: Number of pages in the book
          minimum: 1
          type: integer
        title:
          description: Title of the book
          type: string
        available:
          type: boolean
    AuthorCreateUpdateDTO:
      type: object
      properties:
        age:
          format: int32
          description: Age of the author
          minimum: 1
          type: integer
        authorStatus:
          allOf:
          - $ref: '#/components/schemas/AuthorStatus'
          - description: Current status of the author
        lastName:
          description: Last name of the author
          type: string
        name:
          description: Name of the author
          type: string
